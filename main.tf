# Terraform configuration

terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
    }
  }
}

module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "2.21.0"

  name = var.vpc_name
  cidr = var.vpc_cidr

  azs             = var.vpc_azs
  private_subnets = var.vpc_private_subnets
  public_subnets  = var.vpc_public_subnets

  enable_nat_gateway   = var.vpc_enable_nat_gateway
  enable_dns_hostnames = true
  tags = var.vpc_tags
}

module "sg" {
  source = "terraform-aws-modules/security-group/aws"

  name        = "user-service"
  vpc_id = module.vpc.vpc_id
  ingress_cidr_blocks      = ["10.0.0.0/16"]
  ingress_with_cidr_blocks = [
    {
      from_port   = 27017
      to_port     = 27017
      protocol    = "tcp"
      description = "mongo"
      cidr_blocks = "10.0.0.0/16"
    },
    {
      rule        = "ssh-tcp"
      cidr_blocks = "0.0.0.0/0"
    },
  ]
  egress_with_cidr_blocks = [
    {
      from_port   = 0
      to_port     = 0
      protocol    = "-1"
      description = "ssh"
      cidr_blocks = "0.0.0.0/0"
    },
    {
      rule        = "ssh-tcp"
      cidr_blocks = "0.0.0.0/0"

},
]
}

module "ec2_instance" {
  source  = "terraform-aws-modules/ec2-instance/aws"
  version = "2.12.0"

  name           = "ec2-bastion"
  instance_count = 1

  ami                    = "ami-0892d3c7ee96c0bf7"
  instance_type          = "t2.micro"
  vpc_security_group_ids = [module.sg.security_group_id]
  subnet_id              = module.vpc.public_subnets[0]
  key_name 		 = "oregon"
  iam_instance_profile   = "aws_ec2"
  
  user_data = <<-EOF
#!/bin/bash
sudo apt update
sudo apt upgrade -y
sudo apt install ansible
git clone https://gitlab.com/ashish.jain4/final_ppt.git
cd final_ppt
cp -r ansible.cfg /etc/ansible/ansible.cfg
EOF

  tags = {
    Terraform   = "true"
    Environment = "dev"
  }
}

module "ec2_instances" {
  source  = "terraform-aws-modules/ec2-instance/aws"
  version = "2.12.0"
  count = length(var.vpc_private_subnets)
  
  name		         = "ec2-cluster"
  instance_count 	 = 1
  ami                    = "ami-0892d3c7ee96c0bf7"
  instance_type          = "t2.micro"
  vpc_security_group_ids = [module.sg.security_group_id]
  subnet_id              = module.vpc.private_subnets[count.index]
  key_name		 = "oregon"

  tags = {
    Difference  = var.my_list[count.index]
    Terraform   = "true"
    Environment = "dev"
  }
}

